package olga.gorbatiuk.pizzeria;

import java.util.Arrays;
import java.util.List;
import olga.gorbatiuk.pizzeria.entity.Order;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.service.OrderService;
import olga.gorbatiuk.pizzeria.service.PizzaService;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Olga Gorbatiuk //
 */
public class Main {

    public static void main(String... args) {

        ConfigurableApplicationContext dataContext = new ClassPathXmlApplicationContext("dataConfig.xml");
        ConfigurableApplicationContext appContext = new ClassPathXmlApplicationContext(new String[]{"springConfig.xml"}, dataContext);

        System.out.println("------");
        Arrays.asList(dataContext.getBeanDefinitionNames()).stream().forEach(System.out::println);
        System.out.println("------");
        Arrays.asList(appContext.getBeanDefinitionNames()).stream().forEach(System.out::println);

        PizzaService pizzaService = appContext.getBean("pizzaService", PizzaService.class);
        List<Pizza> pizzas = pizzaService.getAllPizzas();

        OrderService orderService = appContext.getBean("orderService", OrderService.class);;

        Order order1 = orderService.createNewOrder();
        order1.addItems(pizzas.get(0), pizzas.get(1));
        orderService.placeOrder(order1);

        Order order2 = orderService.createNewOrder();
        order2.addItems(pizzas.get(0));
        orderService.placeOrder(order2);

        List<Order> orders = orderService.getAllOrders();
        orders.stream().forEach(System.out::println);

    }
}

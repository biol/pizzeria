package olga.gorbatiuk.pizzeria.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Olga Gorbatiuk
 */
//@OrderAnnotation
@Scope(value = "prototype")
@Component("order")
public class Order {

    private UUID id;
    private LocalDateTime date;
    private String name;
    private List<Pizza> pizzas;
    private BigDecimal price;

    public Order() {
        this.pizzas = new ArrayList<>();
    }

    public Order(UUID id, LocalDateTime date) {
        this.id = id;
        this.date = date;
        this.name = (date.toString() + " " + id.toString());
        this.pizzas = new ArrayList<>();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Order{" + "name=" + name + ", pizzas=" + pizzas + ", price=" + price + '}';
    }

    public void addItems(Pizza pizza) {
        pizzas.add(pizza);
    }

    public void addItems(Pizza... pizza) {
        int length = pizza.length;
        if (length == 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < length; i++) {
            pizzas.add(pizza[i]);

        }
    }

}

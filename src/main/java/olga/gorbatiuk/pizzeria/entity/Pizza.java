package olga.gorbatiuk.pizzeria.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.springframework.stereotype.Component;

/**
 *
 * @author Olga Gorbatiuk
 */
@Component
@Entity
public class Pizza implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private double prise;
    @Enumerated(EnumType.STRING)
    private PizzaType pizzaType;

    public Pizza() {

    }

    public Pizza(String name, double prise, PizzaType pizzaType) {
        this.name = name;
        this.prise = prise;
        this.pizzaType = pizzaType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrise() {
        return prise;
    }

    public void setPrise(double prise) {
        this.prise = prise;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.prise);
        hash = 67 * hash + Objects.hashCode(this.pizzaType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pizza other = (Pizza) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.prise, other.prise)) {
            return false;
        }
        if (this.pizzaType != other.pizzaType) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pizza{" + "name=" + name + ", prise=" + prise + ", pizzaType=" + pizzaType + '}';
    }

}

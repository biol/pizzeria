package olga.gorbatiuk.pizzeria.entity;

import org.springframework.stereotype.Component;

/**
 *
 * @author Olga Gorbatiuk
 */
//@Component
public enum PizzaType {

    PAPERONY, GAWAII, FOURCHEESES;
}

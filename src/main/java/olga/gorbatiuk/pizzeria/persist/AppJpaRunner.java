package olga.gorbatiuk.pizzeria.persist;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.entity.PizzaType;

/**
 *
 * @author Olga Gorbatiuk
 */
public class AppJpaRunner {

    public static void main(String[] args) {
        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("pizza");
        EntityManager entityManager = managerFactory.createEntityManager();

        Pizza pizza = new Pizza();
        pizza.setName("PizzaName");
        pizza.setPizzaType(PizzaType.GAWAII);
        pizza.setPrise(120.00);

        System.out.println("Before: " + pizza);
        entityManager.getTransaction().begin();
        entityManager.persist(pizza);
        entityManager.getTransaction().commit();
        System.out.println("After: " + pizza);
        entityManager.close();

//        EntityManagerFactory managerFactorн1 = Persistence.createEntityManagerFactory("pizza");
        Pizza pizza1 = entityManager.find(Pizza.class, pizza.getId());

        System.out.println(pizza1);
        entityManager.close();
        managerFactory.close();
    }

}

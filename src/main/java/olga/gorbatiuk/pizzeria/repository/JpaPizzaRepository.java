package olga.gorbatiuk.pizzeria.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.entity.PizzaType;

/**
 *
 * @author Olga Gorbatiuk
 */
public class JpaPizzaRepository implements PizzaRepository {

//   @PersistenceContext(name = "pizza");
    private EntityManagerFactory emf;

    @Override
    public List<Pizza> getPizzaByType(PizzaType pizzaType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Pizza> getAllPizzas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer save(Pizza pizza) {
        EntityManager em = emf.createEntityManager();
        em.persist(pizza);
        em.close();
        return pizza.getId();
    }
}

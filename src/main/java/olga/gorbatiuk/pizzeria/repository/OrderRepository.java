/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olga.gorbatiuk.pizzeria.repository;

import java.util.List;
import java.util.UUID;
import olga.gorbatiuk.pizzeria.entity.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Olga Gorbatiuk
 */
@Repository
public interface OrderRepository {

    public UUID getNewOrderId();
    
    public boolean setOrder(Order order);
    
    public List<Order> findAll(); 
}

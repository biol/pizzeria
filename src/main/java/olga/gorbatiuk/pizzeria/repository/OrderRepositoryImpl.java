package olga.gorbatiuk.pizzeria.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import olga.gorbatiuk.pizzeria.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    private List<Order> orders = new ArrayList<>();

//    @Autowired
//    public OrderRepositoryImpl(List<Order> orders) {
//        this.orders = orders;
//    }
    @Override
    public UUID getNewOrderId() {
        return UUID.randomUUID();
    }

    @Override
    public boolean setOrder(Order order) {
        orders.add(order);
        return orders.contains(order);
    }

    @Override
    public List<Order> findAll() {
        return orders;
    }

}

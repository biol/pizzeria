/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olga.gorbatiuk.pizzeria.repository;

import java.util.List;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.entity.PizzaType;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Olga Gorbatiuk
 */
//@Repository
public interface PizzaRepository {

    List<Pizza> getPizzaByType(PizzaType pizzaType);

    List<Pizza> getAllPizzas();

}

package olga.gorbatiuk.pizzeria.repository;

import java.util.ArrayList;
import java.util.List;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.entity.PizzaType;
import org.springframework.beans.factory.annotation.Autowired;

public class PizzaRepositoryImpl implements PizzaRepository {

    private List<Pizza> pizzas;

//    public PizzaRepositoryImpl(List<Pizza> pizzas) {
//        this.pizzas = pizzas;
//    }
    @Override
    public List<Pizza> getAllPizzas() {
        return pizzas;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    @Override
    public List<Pizza> getPizzaByType(PizzaType pizzaType) {
        List<Pizza> pizzasOfOneType = new ArrayList<>();

        // Old style
//        for (Pizza pizza : pizzas) {
//            if (pizza.getPizzaType().equals(pizzaType)) {
//                pizzasOfOneType.add(pizza);
//            }
//        }
        pizzas.stream().filter((pizza) -> (pizza.getPizzaType().equals(pizzaType))).forEach((pizza) -> {
            pizzasOfOneType.add(pizza);
        });
        return pizzasOfOneType;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olga.gorbatiuk.pizzeria.service;

import java.util.List;
import olga.gorbatiuk.pizzeria.entity.Order;
import org.springframework.stereotype.Service;

/**
 *
 * @author Olga Gorbatiuk
 */
//@Service("orderService")
public interface OrderService {

    public Order createNewOrder();

    public boolean placeOrder(Order order1);

    public List<Order> getAllOrders();

}

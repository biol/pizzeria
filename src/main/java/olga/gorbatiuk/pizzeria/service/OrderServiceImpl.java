package olga.gorbatiuk.pizzeria.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import olga.gorbatiuk.pizzeria.entity.Order;
import olga.gorbatiuk.pizzeria.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("orderService")
public abstract class OrderServiceImpl implements OrderService {

//    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order createNewOrder() {
        UUID id = orderRepository.getNewOrderId();
        Order order = createOrder();
        order.setId(id);
        order.setDate(LocalDateTime.now());
        order.setName(id.toString() + " " + LocalDateTime.now().toString());
//        Order order = new Order(id, LocalDateTime.now()); // TODO заинжектить; Посмотреть Lookup method injection 5.4.6 Documentation
        return order;
    }

    abstract Order createOrder();

    @Override
    public boolean placeOrder(Order order) {
        return orderRepository.setOrder(order);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}

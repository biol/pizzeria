/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olga.gorbatiuk.pizzeria.service;

import java.util.List;
import olga.gorbatiuk.pizzeria.entity.Pizza;
import olga.gorbatiuk.pizzeria.entity.PizzaType;
import org.springframework.stereotype.Service;

/**
 *
 * @author Olga Gorbatiuk
 */
//@Service("pizzaService")
public interface PizzaService {

    List<Pizza> getAllPizzas();

    List<Pizza> getPizzaByType(PizzaType pizzaType);
}

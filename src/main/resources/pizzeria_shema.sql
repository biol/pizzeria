-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema pizzeria
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema pizzeria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pizzeria` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `pizzeria` ;

-- -----------------------------------------------------
-- Table `pizzeria`.`pizza_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzeria`.`pizza_type` ;

CREATE TABLE IF NOT EXISTS `pizzeria`.`pizza_type` (
  `pizza_type_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`pizza_type_id`),
  UNIQUE INDEX `pizza_type_id_UNIQUE` (`pizza_type_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzeria`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzeria`.`order` ;

CREATE TABLE IF NOT EXISTS `pizzeria`.`order` (
  `order_id` INT NOT NULL,
  `date` DATETIME NOT NULL,
  `price` DECIMAL NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`order_id`),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pizzeria`.`pizza`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pizzeria`.`pizza` ;

CREATE TABLE IF NOT EXISTS `pizzeria`.`pizza` (
  `pizza_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` DOUBLE NOT NULL,
  `pizza_type_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`pizza_id`),
  UNIQUE INDEX `pizza_id_UNIQUE` (`pizza_id` ASC),
  INDEX `fk__pizza__pizza_type_id_idx` (`pizza_type_id` ASC),
  INDEX `fk__pizza__order_id_idx` (`order_id` ASC),
  CONSTRAINT `fk__pizza__pizza_type_id`
    FOREIGN KEY (`pizza_type_id`)
    REFERENCES `pizzeria`.`pizza_type` (`pizza_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk__pizza__order_id`
    FOREIGN KEY (`order_id`)
    REFERENCES `pizzeria`.`order` (`order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
